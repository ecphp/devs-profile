# Developers profile

Powered by [Home Manager][home-manager website], this project offers a variety
of user profiles, each packed with command-line programs and configurations
designed specifically for developers.

Leveraging the [Nix package manager][nix website], Home Manager provides a
declarative approach to managing a user's environment. It focuses on
user-specific (non-global) packages and dotfiles, offering a "ready-to-use"
profile for command-line enthusiasts. The cornerstone of this project is its
commitment to reproducibility across all machines, ensuring consistent
experiences for users in different environments.

### Compatibility

This configuration has been rigorously tested and confirmed to work seamlessly
on a range of operating systems, including:

- Any Linux
- Amazon SRV4Dev
- Apple MacOS

With this broad compatibility, developers can trust in a uniform setup process
and working environment, regardless of their OS choice.

## Requirements

- [Nix][nix website] package manager,
- No root access required.

## Installation

Install this developer profile in three steps:

1. **Install Nix:** Ensure Nix is installed on your system. You could use the
   the default installation script from
   [nixos.org/download](https://nixos.org/download/) or for a better experience,
   we recommend using [Lix](https://lix.systems/), a fork of Nix with a better
   installer, providing more features and covering more edge cases.

2. **Enable a specific profile:** To apply a profile to your user, execute the
   following command in a terminal:

   ```shell
   nix run home-manager/master -- switch --flake git+https://code.europa.eu/ecphp/devs-profile#light --impure
   ```

   Here, the `light` profile is used as an example. You can replace it with any
   other profile, such as `default`, see the section "Profiles" for more
   information.

   Restart your terminal afterwards.

### Note about `--impure` flag

To use this home-manager profile, you must include the `--impure` flag. This
necessity arises from the fact that the profile is designed to function with any
username on any machine. As usernames vary across different machines, it's
essential to have a method to read the current username within this profile.
This can be accomplished by accessing the environment variable `USER`. However,
by design, a Nix flake does not have permission to access local environment
variables. Adding the `--impure` flag grants this access, ensuring that the
variable can be read and the profile can function as intended.

## Update

The `flake.lock` file in this repository pins all necessary information for a
reproducible and fully functional environment. To update, rerun the installation
command. Nix ensures the user environment matches this repository's state.

## Usage

Once installed, users will have access to the list of software, see section
"What's inside". Some of them are pre-configured, like `fish` and `git`.

## Profiles

### Light

This profile is a lightweight profile, containing only the essentials for a cool
and efficient command-line experience.

- Setup the [`fish`][fish website] shell with:
  - [`direnv`][direnv website]
  - Add [`nix-your-shell`][nix your shell website] to your shell prompt

Install it by doing:

```shell
nix run home-manager/master -- switch --flake git+https://code.europa.eu/ecphp/devs-profile#light --impure
```

### Default

This profiles, the default one, is a more complete profile, containing the
`light` profile and more tools for a complete development experience.

List of tools included:

- [cachix][cachix website]
- [difftastic][difftastic github]
- [dust][dust github]
- [fd][fd github]
- [git][git website] + many custom aliases
- [gnupg][gnupg website]
- [procs][procs github]
- [lazygit][lazygit website]
- [micro][micro website]

Install it by doing:

```shell
nix run home-manager/master -- switch --flake git+https://code.europa.eu/ecphp/devs-profile#default --impure
```

### More profiles?

More profiles will be added in the future, each tailored to a specific use case.
Feel free to suggest new profiles or contribute your own!

### Customisations

#### git

`git` comes with a handful of aliases, to list them use `git aliases`. However,
the default configuration of `git` is managed by `nix` in `~/.config/git/config`
and it's better to not modify this file directly. Therefore, if you want to make
changes in the git configuration, it is advised to create a file `~/.gitconfig`
containing all your local changes, such as:

```ini
[user]
    email = "username@domain"
    name = "John Doe"
```

### Testing

You can test each available profile individually within a temporary virtual
machine using a single command. This project uses [`nixos-generators`], allowing
you to generate a virtual machine for any specific profile you wish to test
using many existing formats (e.g.: QEMU, VirtualBox, etc.).

To test the `light` profile in QEMU, execute the following commands:

```shell
# Replace `light` in the next line with the profile you want to test
nix build .#nixosConfigurations.light.config.system.build.vm --impure
./result/bin/run-nixos-vm
```

Multiple configuration formats are supported. For further details, please
consult the
[Supported Formats](https://github.com/nix-community/nixos-generators#readme)
section in the [`nixos-generators`] documentation.

### Troubleshooting

On macOS, the installation may fail if Xcode is not installed because it
requires `git`. To resolve this issue, run the command
`nix shell nixpkgs#gitMinimal` before starting the installation. This command
provides a minimal version of `git` that is sufficient to complete the
installation. Once installed, the `git` command will be available within the
profile.

[nix website]: https://nixos.org/
[home-manager website]: https://github.com/nix-community/home-manager
[cachix website]: https://www.cachix.org/
[direnv website]: https://direnv.net/
[dust github]: https://github.com/bootandy/dust
[nix-direnv github]: https://github.com/nix-community/nix-direnv
[eza github]: https://github.com/eza-community/eza
[fd github]: https://github.com/sharkdp/fd
[fish website]: https://fishshell.com/
[git website]: https://git-scm.com/
[gnupg website]: https://gnupg.org/
[difftastic github]: https://github.com/Wilfred/difftastic
[htop website]: https://htop.dev/
[procs github]: https://github.com/dalance/procs
[ripgrep github]: https://github.com/BurntSushi/ripgrep
[lazygit website]: https://github.com/jesseduffield/lazygit
[determinate systems installer post]:
  https://determinate.systems/posts/determinate-nix-installer
[home manager manual]: https://nix-community.github.io/home-manager/
[starfish website]: https://starship.rs/
[zoxide website]: https://github.com/ajeetdsouza/zoxide
[micro website]: https://micro-editor.github.io/
[`nixos-generators`]: https://github.com/nix-community/nixos-generators
[nix your shell website]: https://github.com/MercuryTechnologies/nix-your-shell
