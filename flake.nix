{
  description = "A Nix flake for European Commission development environments using Nix and Home Manager";

  inputs = {
    nixpkgs.url = "github:/nixos/nixpkgs/nixpkgs-unstable";
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    flake-parts.url = "github:hercules-ci/flake-parts";
    systems.url = "github:nix-systems/default";
    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    inputs@{ flake-parts, ... }:
    flake-parts.lib.mkFlake { inherit inputs; } ({
      systems = import inputs.systems;

      imports = [
        ./imports/formatter.nix
        ./imports/homeConfigurations.nix
        ./imports/nixosConfigurations.nix
        ./imports/devShells.nix
        ./imports/lib.nix
      ];
    });
}
