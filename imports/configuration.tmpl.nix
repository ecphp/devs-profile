# configuration.nix
{
  username,
  lib,
  ...
}:

{
  # only add strictly necessary modules
  boot.initrd.includeDefaultModules = false;
  boot.initrd.kernelModules = [ "ext4" ];

  # disable useless software
  environment.defaultPackages = [ ];
  xdg.icons.enable = false;
  xdg.mime.enable = false;
  xdg.sounds.enable = false;

  system.stateVersion = "24.05";

  services.getty.autologinUser = lib.mkForce username;

  users.users = {
    "${username}" = {
      isNormalUser = true;
      extraGroups = [ "wheel" ];
      password = username;
    };
  };

  services.openssh = {
    enable = true;
  };
}
