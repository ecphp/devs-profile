{
  inputs,
  self,
  lib,
  ...
}:

{
  flake.nixosConfigurations =
    let
      username = "admin";
      homeDirectory = "/home/admin";

      profiles =
        builtins.map
          (set: {
            name = "${set.profile}${lib.optionalString (set.system != "") "-${set.system}"}";
            value = let
              system = if set.system != "" then set.system else builtins.currentSystem;
              pkgs = import inputs.nixpkgs {
                inherit system;
              };
            in inputs.nixpkgs.lib.nixosSystem {
              inherit system;
              specialArgs = {
                inherit (set) profile;
                inherit inputs system pkgs;
              };
              modules = [
                inputs.home-manager.nixosModules.home-manager
                inputs.nixos-generators.nixosModules.all-formats
                ({
                  home-manager = {
                    extraSpecialArgs = {
                      inherit (set) profile;
                      system = if set.system != "" then set.system else builtins.currentSystem;
                      inherit inputs username homeDirectory;
                    };
                    useGlobalPkgs = true;
                    useUserPackages = true;
                    users."${username}".imports = self.lib.umport {
                      paths = [
                        ../profiles/base
                        (../profiles + "/${set.profile}")
                      ];
                    };
                  };
                })
                (import ./configuration.tmpl.nix {
                  inherit (set) profile;
                  system = if set.system != "" then set.system else builtins.currentSystem;
                  inherit
                    inputs
                    username
                    homeDirectory
                    lib
                    pkgs
                    ;
                })
              ];
            };
          })
          (
            lib.cartesianProduct {
              system = ([ builtins.currentSystem ] ++ (import inputs.systems) ++ [ "" ]);
              profile = builtins.map (p: p.name) (
                lib.attrsToList (
                  lib.filterAttrs (k: v: v == "directory" && k != "base") (builtins.readDir ../profiles)
                )
              );
            }
          );
    in
    builtins.listToAttrs profiles;
}
