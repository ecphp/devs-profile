{
  inputs,
  self,
  lib,
  ...
}:
{
  flake.homeConfigurations =
    let
      isImpure = builtins ? currentSystem;
      # This is why we have to use --impure flag.
      usernameEnvVar = builtins.getEnv "USER";
      homeEnvVar = builtins.getEnv "HOME";
      # End of impure part.

      # If the user cannot be found, we cannot continue.
      username =
        if "" == usernameEnvVar then throw "Unable to detect username, aborting." else usernameEnvVar;
      # If the home directory cannot be found, we cannot continue.
      homeDirectory =
        if "" == homeEnvVar then throw "Unable to detect user home directory, aborting." else homeEnvVar;

      profiles =
        builtins.map
          (set: {
            name = "${set.profile}${lib.optionalString (set.system != "") "-${set.system}"}";
            value = inputs.home-manager.lib.homeManagerConfiguration {
              pkgs = import inputs.nixpkgs { };
              modules = self.lib.umport {
                paths = [
                  (./. + "/../profiles/base/")
                  (./. + "/../profiles/${set.profile}/")
                ];
              };
              extraSpecialArgs = {
                inherit (set) profile;
                system = if set.system != "" then set.system else builtins.currentSystem;
                inherit inputs username homeDirectory;
              };
            };
          })
          (
            lib.cartesianProduct {
              system = ([ builtins.currentSystem ] ++ (import inputs.systems) ++ [ "" ]);
              profile = builtins.map (p: p.name) (
                lib.attrsToList (
                  lib.filterAttrs (k: v: v == "directory" && k != "base") (builtins.readDir ../profiles)
                )
              );
            }
          );
    in

    if false == isImpure then
      throw "This home manager profile requires the use of the `--impure` flag. Please refer to the documentation at https://code.europa.eu/ecphp/devs-profile#note-about-impure-flag to learn more about this requirement."
    else
      builtins.listToAttrs profiles;
}
