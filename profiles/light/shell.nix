{ pkgs, ... }:

{
  programs = {
    direnv = {
      enable = true;
      config = {
        global = {
          hide_env_diff = true;
        };
        whitelist.prefix = [ "/workspaces" ];
      };
      nix-direnv.enable = true;
    };
    fish = {
      enable = true;
      functions = {
        fish_greeting = "";
      };
      # Optimize Fish in size (220M to 120M)
      # See https://github.com/NixOS/nixpkgs/pull/304313
      # package = pkgs.fish.override { python3 = pkgs.python3Minimal; };
    };
    nix-your-shell = {
      enable = true;
    };
  };
}
