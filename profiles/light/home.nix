{ ... }:

{
  # To optimize the profile size, disable `systemd`
  # See: https://github.com/nix-community/home-manager/issues/5274
  systemd.user.enable = false;
}
