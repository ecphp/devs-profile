{ ... }:

{
  home.file = {
    ".face" = {
      source = ./files/home/avatar.png;
      recursive = true;
    };
    ".face.icon" = {
      source = ./files/home/avatar.png;
      recursive = true;
    };
    "Code/" = {
      source = ./files/home/Code;
      recursive = true;
    };
  };
}
