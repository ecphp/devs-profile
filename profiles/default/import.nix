{ inputs, ... }:
{
  # Import the `light` profile
  imports = inputs.self.lib.umport { path = ./. + "../../light"; };
}
