{ pkgs, ... }:

{
  home.packages = with pkgs; [
    cachix
    du-dust
    fd
    procs
    wget
  ];
}
