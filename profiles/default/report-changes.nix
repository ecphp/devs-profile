{ lib, ... }:
{
  programs.report-changes.enable = lib.mkForce true;
}
