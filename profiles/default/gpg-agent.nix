{ lib, system, ... }:

{
  services = lib.optionalAttrs (system == "x86_64-linux") {
    gpg-agent = {
      enable = true;
      enableSshSupport = true;
    };
  };
}
