{
  config,
  pkgs,
  lib,
  ...
}:

let
  cfg = config.programs.report-changes;
in
{
  options.programs.report-changes = {
    enable = lib.mkEnableOption "Enable reporting with NVD";
  };

  config = lib.mkIf cfg.enable ({
    home.activation.profile-report-changes = config.lib.dag.entryAnywhere ''
      if [[ -v oldGenPath ]]; then
        ${lib.getExe pkgs.nvd} diff $oldGenPath $newGenPath
      fi
    '';
  });
}
