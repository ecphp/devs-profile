{
  pkgs,
  username,
  homeDirectory,
  ...
}:

{
  home = {
    inherit username homeDirectory;
    stateVersion = "24.05";
  };

  # We don't need to install `home-manager`
  programs.home-manager.enable = false;

  # Mostly for saving space
  i18n.glibcLocales = pkgs.glibcLocalesUtf8;

  xdg.enable = true;

  news.display = "silent";
}
